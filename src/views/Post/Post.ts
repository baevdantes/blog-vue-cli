import {Component, Vue} from "vue-property-decorator"
import {namespace} from "vuex-class";
import VueMarkdown from 'vue-markdown';

const PostNs = namespace("post");
const userNs = namespace("users");

@Component({
	components: {
		VueMarkdown
	},
})
export default class Post extends Vue {
	@PostNs.Action("getPostGraph") postGraphResponse: any;
	@PostNs.Action("postCommentGraph") postCommentGr: any;
	@PostNs.Getter("post") postResponse: any;
    @userNs.Getter('getUser') user: any;

	commentValue: string = "";
	nameValue: string = "";

    created() {
        this.postGraphResponse(this.postID);
    }

	get dataLoginComment() {
		return {
            userID: this.user._id,
			text: this.commentValue,
			postID: this.postID,
		}
	}

    get dataUnauthComment() {
        return {
            text: this.commentValue,
            postID: this.postID,
            author: this.nameValue
        }
    }


    get sortedComments() {
		return this.postResponse.post.comments;
	}

	get validatedComment(): boolean {
    	if (this.user) {
    		if (this.commentValue) {
    			return true
			}
		} else {
    		if (this.commentValue && this.nameValue) {
    			return true
			}
		}

		return false;
	}

	get postID(): string {
		return this.$route.params.id;
	}

	postThisComment() {
    	if (this.user !== null) {
            this.postCommentGr(this.dataLoginComment);
		} else {
            this.postCommentGr(this.dataUnauthComment);
		}
		this.commentValue = "";
		this.nameValue = "";
	}
}

