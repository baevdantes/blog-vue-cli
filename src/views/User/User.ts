import {Component, Vue, Watch} from "vue-property-decorator";
import {namespace} from "vuex-class";
import {UserInterface} from "@/store/interfaces/users.interface";
import router from "@/router";

const userNs = namespace("users");

@Component

export default class User extends Vue {
	@userNs.Getter('getUser') user: UserInterface;

	@Watch('user') goToRoute() {
		if (this.user === null) {
			router.go(-1);
		}
	}
}