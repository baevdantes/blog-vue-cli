import {Component, Vue} from "vue-property-decorator";
import {namespace} from "vuex-class";
import LoginForm from "@/components/LoginForm/LoginForm"
import RegistrationForm from "@/components/RegistrationForm/RegistrationForm"

const UserNs = namespace("users");

const regexMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


interface Tab {
    name: string;
    isActive: boolean;
}

interface Tabs {
    tabs: Tab[]
}

@Component({
    components: {
        LoginForm,
        RegistrationForm
    }
})
export default class Login extends Vue {
    @UserNs.Action("registerUser") registrationResponse: any;

    userName: string = "";
    userMail: string = "";
    userPassword: string = "";

    changeTab(name: string) {
        this.userTabs.tabs.forEach((item) => {
            item.isActive = false;
        });
        this.userTabs.tabs.find((item) => {
            return item.name === name;
        }).isActive = true;
    }

    userTabs: Tabs = {
        tabs: [
            {
                name: "Login",
                isActive: true,
            },
            {
                name: "Registration",
                isActive: false,
            }
        ]
    };

    get userData() {
        return {
            username: this.userName,
            email: this.userMail,
            password: this.userPassword
        }
    }

    get isValidateMailRegister() {
        return regexMail.test(String(this.userMail).toLowerCase());
    }

    get isValidateFormRegister() {
        return this.userName
            && this.userName.length >= 3
            && this.userMail
            && this.isValidateMailRegister
            && this.userPassword
            && this.userPassword.length >= 6
    }

    registerUser() {
        this.registrationResponse(this.userData);
    }
}

