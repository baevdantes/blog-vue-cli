import Vue from "vue";
import Vuex from "vuex";
import posts from "./modules/Posts"
import post from "./modules/Post"
import users from "./modules/Users"

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        posts,
        post,
        users
    },
    state: {},
    mutations: {},
    actions: {}
});
