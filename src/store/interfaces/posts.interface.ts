export interface PostInterface {
    id: string;
    title: string;
}

export default interface PostsInterface {
    posts: PostInterface[];
    isLoadingPosts: boolean;
}
