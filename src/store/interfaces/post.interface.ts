export interface UserComment {
    _id: string;
    username: string;
}

export interface CommentInterface {
    user: UserComment;
    text: string;
    author: string;
    createdAt: string;
}

export default interface PostInterface {
    post: {
        title: string;
        text: string;
        _id: string;
        comments: CommentInterface[];
    };
    isLoadingComment: boolean;
    isLoadingPost: boolean;
}