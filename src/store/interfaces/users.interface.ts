export interface UserInterface {
    username: string;
    email: string;
    password: string;
    role: string;
    id: string;
}

export default interface UsersInterface {
	users: UserInterface[];
	user: UserInterface;
    isLoadingRegistration: boolean;
}