import 'whatwg-fetch';
import {ActionContext} from "vuex";
import {UserInterface} from "@/store/interfaces/users.interface";
import router from "@/router";


export const actions = {
	registerUser({state, commit}: ActionContext<any, any>, userData: UserInterface) {
		fetch('http://localhost:1337/auth/local/register', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(userData)
		}).then((res) => res.json())
			.then((data) => {
				if (!data.error) {
					commit("setUser", data.user);
					localStorage.setItem("JWT", data.jwt);
				}
			}).then(() => {
            router.go(-1);
		})
			.catch((err) => console.error(err))
	},

	logout({state, commit, dispatch}: ActionContext<any, any>) {
		commit("logoutUser");
	},

	authUser({state, commit}: ActionContext<any, any>) {
		fetch('http://localhost:1337/users/me', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem("JWT")}`
			}
		}).then((res) => res.json())
			.then((data) => {
				if (!data.error) {
					commit("setUser", data);
				} else {
					commit("logoutUser");
				}
			})
			.catch((err) => console.error(err))
	},

	loginUser({state, commit}: ActionContext<any, any>, userData: any) {
		fetch('http://localhost:1337/auth/local', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(userData)
		}).then((res) => res.json())
			.then((data) => {
				if (!data.error) {
					commit("setUser", data.user);
					localStorage.setItem("JWT", data.jwt);
                    router.go(-1);
				}
			})
			.catch((err) => console.error(err))
	},
};


