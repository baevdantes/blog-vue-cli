import UsersInterface from "@/store/interfaces/users.interface";
import {UserInterface} from "@/store/interfaces/users.interface";

export const mutations = {
	setUser(state: UsersInterface, user: UserInterface) {
		state.user = user;
	},

	setUserLoading(state: any, payload: boolean) {
		state.isLoadingUserPage = payload;
	},

	logoutUser(state: UsersInterface) {
        state.user = null;
        localStorage.setItem("JWT", null);
	}
};
