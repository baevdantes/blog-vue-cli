import UsersInterface from "@/store/interfaces/users.interface";

export const getters = {
	getUsers: (state: UsersInterface) => state.users,
	getUser: (state: UsersInterface) => state.user,
};
