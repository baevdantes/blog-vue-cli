import UsersInterface from "@/store/interfaces/users.interface";

export const state: UsersInterface = {
	users: [],
	user: null,
	isLoadingRegistration: false
};
