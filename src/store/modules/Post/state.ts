import PostInterface from "@/store/interfaces/post.interface.ts"

export const state: PostInterface = {
    post: {
        text: "",
        title: "",
        _id: "",
        comments: []
    },
    isLoadingPost: false,
    isLoadingComment: false,
};
