import 'whatwg-fetch';
import {ActionContext} from "vuex";

export const actions = {
    getPostGraph({state, commit}: ActionContext<any, any>, postId: string) {
        const $queryPost = `{ post (id: "${postId}") { 
                                title 
                                text 
                                _id
                                comments (sort: "createdAt:desc") {
                                  user {
                                    username
                                    _id
                                  }
                                  author
                                  text
                                  createdAt
                                } 
                                   } 
                                      }`;

        commit("setPostLoading", true);

        fetch(`http://localhost:1337/graphql`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({query: $queryPost}),
        })
            .then(function (response) {
                return response.json()
            })
            .then((response) => {
                commit("fillPost", response.data.post);
            })
            .catch(function (ex) {
                console.log('parsing failed', ex)
            }).finally(() => {
            setTimeout(() => commit("setPostLoading", false), 300)
        });
    },

    postCommentGraph({state, commit}: ActionContext<any, any>, dataComment: any) {
        const userID = dataComment.userID;
        const postID = dataComment.postID;
        const text = dataComment.text;
        const author = dataComment.author;

        const $queryUnauthComment = `mutation {
                                  createComment (input: {
                                    data: {
                                      post: "${postID}",
                                      text: "${text}",
                                      author: "${author}"
                                    }
                                  }){
                                    comment {
                                      user {
                                        username
                                        _id
                                      }
                                      author
                                      createdAt
                                      text
                                    }
                                  }
                                }`;

        const $queryLoginComment = `mutation {
                                  createComment (input: {
                                    data: {
                                      user: "${userID}",
                                      post: "${postID}",
                                      text: "${text}",
                                    }
                                  }){
                                    comment {
                                      user {
                                        username
                                        _id
                                      }
                                      createdAt
                                      text
                                    }
                                  }
                                }`;

        if (dataComment.userID) {
            commit("setCommentLoading", true);
            fetch('http://localhost:1337/graphql', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({query: $queryLoginComment}),
            }).then(function (response) {
                return response.json()
            }).then((data) => {
                setTimeout(() => {
                    commit("addComment", data);
                }, 300);
            })
                .catch((err) => console.error(err))
                .finally(() => {
                    setTimeout(() => {
                        commit("setCommentLoading", false);
                    }, 300)
                })
        } else {
            commit("setCommentLoading", true);
            fetch('http://localhost:1337/graphql', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({query: $queryUnauthComment}),
            }).then(function (response) {
                return response.json()
            }).then((data) => {
                setTimeout(() => {
                    commit("addComment", data);
                }, 300);
            })
                .catch((err) => console.error(err))
                .finally(() => {
                    setTimeout(() => {
                        commit("setCommentLoading", false);
                    }, 300)
                })
        }
    }
};
