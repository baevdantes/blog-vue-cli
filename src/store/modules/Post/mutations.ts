import PostInterface from "@/store/interfaces/post.interface";


export const mutations = {
	fillPost(state: PostInterface, payload: any) {
		state.post = payload;
	},

	addComment(state: PostInterface, payload: any) {
        state.post.comments.unshift(payload.data.createComment.comment);
    },

	setCommentLoading(state: PostInterface, payload: boolean) {
		state.isLoadingComment = payload;
	},

	setPostLoading(state: PostInterface, payload: boolean) {
		state.isLoadingPost = payload;
	}
};
