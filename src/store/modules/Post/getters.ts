import PostInterface from "@/store/interfaces/post.interface";

export const getters = {
    post: (state: PostInterface) => state,
};
