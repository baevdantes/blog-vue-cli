import {default as PostsInterface} from "@/store/interfaces/posts.interface";

export const mutations = {
    setPostsLoading(state: PostsInterface, payload: boolean) {
        state.isLoadingPosts = payload;
    },

    fillPosts(state: PostsInterface, payload: any) {
        state.posts = payload;
    }
};
