import PostsInterface from "@/store/interfaces/posts.interface.ts"

export const getters = {
  posts: (state: PostsInterface) => state.posts,
  isLoadingPosts: (state: PostsInterface) => state.isLoadingPosts,
};
