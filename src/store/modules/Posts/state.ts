import PostsInterface from "@/store/interfaces/posts.interface.ts"

export const state: PostsInterface = {
    posts: [],
    isLoadingPosts: false
};
