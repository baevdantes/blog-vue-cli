import 'whatwg-fetch';
import {ActionContext} from "vuex";

export const actions = {
    getPosts({state, commit}: ActionContext<any, any>, postId: string) {
        const $queryPosts = `{
								  posts (sort: "createdAt:desc") {
									title
									_id
								  }
								}`;

        commit("setPostsLoading", true);

        fetch(`http://localhost:1337/graphql`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({query: $queryPosts}),
        })
            .then(function (response) {
                return response.json()
            })
            .then((response) => {
                commit("fillPosts", response.data.posts);
            })
            .catch(function (ex) {
                console.log('parsing failed', ex)
            }).finally(() => {
                setTimeout(() => {
                    commit("setPostsLoading", false);
                }, 300)
        })
    },
};
