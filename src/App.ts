import { Component, Vue } from 'vue-property-decorator';
import Header from '@/components/Header/Header';
import Footer from '@/components/Footer/Footer';

@Component({
    components: {
        Header,
        Footer
    }
})
export default class App extends Vue {

}