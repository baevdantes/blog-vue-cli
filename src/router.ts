import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/views/Home/Home';
import Post from '@/views/Post/Post';
import Login from "@/views/Login/Login";
import User from "@/views/User/User";
import {VueRouter} from "vue-router/types/router";
import { Route } from "vue-router";
import store from "@/store";


Vue.use(Router);

const router: VueRouter = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
        },
        {
            path: '/post/:id',
            name: 'post',
            component: Post,
            props: true
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
        },
        {
            path: '/user',
            name: 'User',
            component: User,
        },
        {
            path: '/about',
            name: 'about',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "about" */ './views/Home/Home.vue'),
        },
    ],

    scrollBehavior (to: Route, from: Route, savedPosition) {
        return { x: 0, y: 0 }
    }
});

router.beforeEach((to: Route, from: Route, next: () => void): any => {
	  store.dispatch("users/authUser").then(() => {
	      next();
		});
});

export default router;