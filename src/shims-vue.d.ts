declare module '*.vue' {
  import Vue from 'vue';
  export default Vue;
}

declare module "vuex-class";
declare module "whatwg-fetch";
declare module "vue-markdown";
