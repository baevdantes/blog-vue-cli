import {Component, Vue, Prop} from "vue-property-decorator";
import {namespace} from "vuex-class";

const regexMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const UserNs = namespace("users");

@Component

export default class RegistrationForm extends Vue {
    @Prop() tab: any;
    @UserNs.Action("registerUser") registrationResponse: any;

    userName: string = "";
    userMail: string = "";
    userPassword: string = "";


    get userData() {
        return {
            username: this.userName,
            email: this.userMail,
            password: this.userPassword
        }
    }

    get isValidateMailRegister() {
        return regexMail.test(String(this.userMail).toLowerCase());
    }

    get isValidateFormRegister() {
        return this.userName
            && this.userName.length >= 3
            && this.userMail
            && this.isValidateMailRegister
            && this.userPassword
            && this.userPassword.length >= 6
    }

    registerUser() {
        this.registrationResponse(this.userData);
    }
}