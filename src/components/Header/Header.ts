import {Vue, Component} from "vue-property-decorator";
import {namespace} from "vuex-class";
import {UserInterface} from "@/store/interfaces/users.interface";

const userNs = namespace("users");

@Component

export default class Header extends Vue {
    @userNs.Getter('getUser') user: UserInterface;
    @userNs.Action('logout') logoutUser: any;
}
