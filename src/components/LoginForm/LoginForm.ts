import {Component, Vue, Prop} from "vue-property-decorator";
import {namespace} from "vuex-class";

const userNs = namespace('users');

const regexMail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;


@Component

export default class LoginForm extends Vue {
    @Prop() tab: any;

    @userNs.Action("loginUser") loginResponse: any;

    userMailLogin: string = "";
    userPasswordLogin: string = "";

    get isValidateFormLogin() {
        return this.isValidateMailLogin
            && this.userPasswordLogin
            && this.userPasswordLogin.length >= 6
    }

    get isValidateMailLogin() {
        return regexMail.test(String(this.userMailLogin).toLowerCase());
    }

    get userDataLogin() {
        return {
            identifier: this.userMailLogin,
            password: this.userPasswordLogin
        }
    }

    login() {
        this.loginResponse(this.userDataLogin);
    }
}