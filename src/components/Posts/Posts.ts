import Post from "@/components/Posts/Post/Post"
import {Vue, Component, Prop} from "vue-property-decorator";
import {namespace} from "vuex-class";
import PostsInterface from "@/store/interfaces/posts.interface";

const PostsNs = namespace("posts");

@Component({
    components: {
        Post
    }
})
export default class Posts extends Vue {
    @PostsNs.Getter("posts") posts: PostsInterface;
    @PostsNs.Getter("isLoadingPosts") isLoadingPosts: boolean;
    @PostsNs.Action("getPosts") postsResponse: any;

    mounted() {
        this.postsResponse();
    }
}