import { Vue, Component, Prop } from 'vue-property-decorator'
import {PostInterface} from "@/store/interfaces/posts.interface";

@Component
export default class Post extends Vue {
	@Prop() post: PostInterface
}
